from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from .models import Question, Choice
from django.views import generic
from django.utils import timezone
from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from .serializer import UserSerializers, GroupSerializer
import polls.serializer
from rest_framework import status
from rest_framework.decorators import api_view,action
from rest_framework.response import Response
from rest_framework import filters
from rest_framework.views import APIView

# Create your views here.
class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'question_list'

    def get_queryset(self):
        return Question.objects.filter(pub_date__lte=timezone.now()).order_by('-pub_date')[:5]
        # return Question.objects.order_by('pub_date')[:5]

class DetailView(generic.DetailView):
    model = Question
    template_name = 'polls/detail.html'

class ResultsView(generic.DetailView):
    model = Question
    template_name = 'polls/results.html'

'''
def index(request):
    question_list = Question.objects.order_by('pub_date')[:5]
    weather_list = Weather.objects.order_by('city')[:5]
    # template = loader.get_template('polls/index.html')
    context = {
        'question_list':question_list,
    }
    # return HttpResponse(template.render(context, request))
    return render(request, 'polls/index.html', context)

def detail(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'polls/detail.html', {'question':question})

def results(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'polls/results.html', {'question':question})
'''

def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        return render(request, 'polls/detail.html', {
            'question': question,
            'error_message': "You didn't select a choice.",
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))

def hello(request):

    return render(request,"report-template.html")

#restful api
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by('date_joined')
    serializer_class = UserSerializers
    # def get(self, request, format=None):
    #     usernames = [user.username for user in User.objects.all()]
    #     return Response(usernames)

class GroupViewSet(viewsets.ModelViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer

class QuestionViewSet(viewsets.ModelViewSet):
    search_fields = ['question_text']
    filter_backends = (filters.SearchFilter,)
    queryset = Question.objects.all()
    serializer_class = polls.serializer.QuestionSerializer

    @action(methods=["post"], detail=False, url_path="postquery")
    def query_list(self, request):
        print(request.data)
        queryset = self.filter_queryset(self.get_queryset())
        queryset = queryset.filter(name=request.data["name"])
        serializer = self.get_serializer(queryset, many=True)
        print(serializer.data)
        return  Response(serializer.data)

@api_view(['GET','POST'])
def question_list(request):
    if request.method == 'GET':
        questions = Question.objects.all()
        serializer = polls.serializer.QuestionSerializer(questions, many=True)
        return Response(serializer.data)
    elif request.method == 'POST':
        serializer = polls.serializer.QuestionSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response("qqqqq", status=status.HTTP_400_BAD_REQUEST)