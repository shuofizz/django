from django.db import models


class Weather(models.Model):
    city = models.CharField(max_length=80, blank=True, null=True)
    temp_lo = models.IntegerField(blank=True, null=True)
    temp_hi = models.IntegerField(blank=True, null=True)
    prcp = models.FloatField(blank=True, null=True)
    date = models.DateField(blank=True, null=True)

    def __str__(self):
        return self.city

    class Meta:
        managed = False
        db_table = 'weather'