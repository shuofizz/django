# Generated by Django 2.2.5 on 2019-11-04 11:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('TestModel', '0001_initial')
    ]

    operations = [
        migrations.CreateModel(
            name='Weather',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('city', models.CharField(blank=True, max_length=80, null=True)),
                ('temp_lo', models.IntegerField(blank=True, null=True)),
                ('temp_hi', models.IntegerField(blank=True, null=True)),
                ('prcp', models.FloatField(blank=True, null=True)),
                ('date', models.DateField(blank=True, null=True)),
            ],
            options={
                'db_table': 'weather',
                'managed': False,
            },
        ),
        migrations.DeleteModel(
            name='Test',
        ),
    ]
